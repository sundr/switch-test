### Dependencies
* PHP v7.1 and higher
* Composer

### Installation
1. git clone https://bitbucket.org/sundr/switch-test.git
1. cd switch-test
1. composer install

### Launching
Code should be ran through command line interface. There are 2 required options: **--time**, **--genre**

1. cd switch-test
1. php index.php --time=7:00 --genre="Drama"

### Testing
1. cd switch-test
1. ./vendor/bin/phpunit --testdox