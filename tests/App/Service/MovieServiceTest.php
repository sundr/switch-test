<?php

namespace Test\App\Service;

use App\Model\Movie;
use App\Service\MovieService;
use PHPUnit\Framework\TestCase;

class MovieServiceTest extends TestCase
{
    private const GENRE = 'animation';
    private const TIME  = '12:00';

    public function testMoviesRecommendationsByGenre(): void
    {
        $movieService = new MovieService([]);
        $movies = self::getMockApiResponse();
        $recommendations = $movieService->getMoviesRecommendations($movies, self::GENRE, self::TIME);

        foreach ($recommendations as $recommendation) {
            $genres = [];
            foreach ($recommendation['movie']->genres as $item) {
                $genres[] = strtolower($item);
            }

            $this->assertContains(self::GENRE, $genres);
        }
    }

    public function testMoviesRecommendationsByTime(): void
    {
        $movieService = new MovieService([]);
        $movies = self::getMockApiResponse();
        $recommendations = $movieService->getMoviesRecommendations($movies, self::GENRE, self::TIME);
        $searchDateTime = new \DateTime(self::TIME);
        $searchDateTime->modify('+30 minutes');

        foreach ($recommendations as $recommendation) {
            $this->assertGreaterThanOrEqual($searchDateTime, $recommendation['showing']);
        }
    }

    public function testMoviesRecommendationsSortedByRating(): void
    {
        $movieService = new MovieService([]);
        $movies = self::getMockApiResponse();
        $recommendations = $movieService->getMoviesRecommendations($movies, self::GENRE, self::TIME);

        $this->assertCount(2, $recommendations);
        $this->assertGreaterThanOrEqual($recommendations[1]['movie']->rating, $recommendations[0]['movie']->rating);
    }

    public function testMoviesRecommendationsOutputString(): void
    {
        $movieService = new MovieService([]);
        $movies = self::getMockApiResponse();
        $recommendations = $movieService->getMoviesRecommendations($movies, self::GENRE, self::TIME);
        $outputString = 'Zootopia, showing at 7pm' . PHP_EOL . 'Shaun The Sheep, showing at 7pm' . PHP_EOL;

        $this->assertCount(2, $recommendations);
        $this->assertEquals($recommendations[0]['movie']->name, 'Zootopia');
        $this->assertEquals($recommendations[1]['movie']->name, 'Shaun The Sheep');
        $this->assertEquals($outputString, MovieService::getRecommendationsOutputString($recommendations));
    }

    public function testEmptyRecommendationsOutputString(): void
    {
        $this->assertEquals(MovieService::EMPTY_RESULT_MESSAGE, MovieService::getRecommendationsOutputString([]));
    }

    /**
     * @return array|Movie[]
     */
    private static function getMockApiResponse(): array
    {
        $json = '[
            {
                "name": "The Martian",
                "rating": 92,
                "genres": [
                    "Science Fiction & Fantasy"
                ],
                "showings": [
                    "17:30:00+11:00",
                    "19:30:00+11:00"
                ]
            },
            {
                "name": "Moonlight",
                "rating": 98,
                "genres": [
                    "Drama"
                ],
                "showings": [
                    "18:30:00+11:00",
                    "20:30:00+11:00"
                ]
            },
            {
                "name": "Shaun The Sheep",
                "rating": 80,
                "genres": [
                    "Animation",
                    "Comedy"
                ],
                "showings": [
                    "19:00:00+11:00"
                ]
            },
            {
                "name": "Zootopia",
                "rating": 92,
                "genres": [
                    "Action & Adventure",
                    "Animation",
                    "Comedy"
                ],
                "showings": [
                    "19:00:00+11:00",
                    "21:30:00+11:00"
                ]
            }
        ]';

        $apiResponse = json_decode($json, true);
        $movies = [];

        foreach ($apiResponse as $item) {
            $movies[] = new Movie($item);
        }

        return $movies;
    }
}
