<?php

namespace App\Model;

class Movie
{
    /** @var string */
    public $name;

    /** @var int */
    public $rating;

    /** @var array */
    public $genres;

    /** @var array */
    public $showings;

    public function __construct(array $data)
    {
        $this->name = $data['name'] ?? null;
        $this->rating = $data['rating'] ?? null;
        $this->genres = $data['genres'] ?? [];
        $this->showings = [];

        if (isset($data['showings'])) {
            foreach ($data['showings'] as $showing) {
                $this->showings[] = new \DateTime($showing);
            }
        }
    }
}
