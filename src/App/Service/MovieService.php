<?php

namespace App\Service;

use Api\JsonApi;
use App\Model\Movie;

class MovieService
{
    public const EMPTY_RESULT_MESSAGE = 'no movie recommendations';

    /** @var array */
    private $configs;

    public function __construct(array $configs)
    {
        $this->configs = $configs;
    }

    /**
     * @return array|Movie[]
     */
    public function fetchMovies(): array
    {
        $movies = [];

        $api = new JsonApi($this->configs);

        foreach ($api->sendRequest('/cVyp3McN') as $item) {
            $movies[] = new Movie($item);
        }

        return $movies;
    }

    /**
     * @param array|Movie[] $movies
     * @param string        $genre
     * @param string        $time
     *
     * @return array
     */
    public function getMoviesRecommendations(array $movies, string $genre, string $time): array
    {
        $recommendations = [];
        $searchDateTime = new \DateTime($time);
        $searchDateTime->modify('+30 minutes');

        foreach ($movies as $movie) {
            $genres = [];
            foreach ($movie->genres as $item) {
                $genres[] = strtolower($item);
            }

            if (in_array(strtolower($genre), $genres)) {
                foreach ($movie->showings as $showingDateTime) {
                    if ($searchDateTime <= $showingDateTime) {
                        $recommendations[] = [
                            'movie'   => $movie,
                            'showing' => $showingDateTime,
                        ];
                        break;
                    }
                }
            }
        }

        return $this->sortRecommendationsByRating($recommendations);
    }

    public static function getRecommendationsOutputString(array $recommendations): string
    {
        $output = '';

        if ($recommendations) {
            foreach ($recommendations as $recommendation) {
                $output .= $recommendation['movie']->name
                    . ', showing at ' . str_replace(':00', '', $recommendation['showing']->format('g:ia')) . PHP_EOL;
            }
        } else {
            $output = self::EMPTY_RESULT_MESSAGE;
        }

        return $output;
    }

    private function sortRecommendationsByRating(array $recommendations): array
    {
        usort($recommendations, function ($first, $second) {
            return $first['movie']->rating < $second['movie']->rating;
        });

        return $recommendations;
    }
}
