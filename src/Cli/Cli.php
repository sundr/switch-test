<?php

namespace Cli;

class Cli
{
    public function parseInputArguments(array $inputArgumentsNames): array
    {
        $argv = $_SERVER['argv'];
        unset($argv[0]);

        $inputArguments = [];

        foreach ($inputArgumentsNames as $name) {
            $issetArgument = false;

            foreach ($argv as $argument) {
                if (strripos($argument, $name) !== false) {
                    $inputArguments[] = str_replace('--' . $name . '=', '', $argument);
                    $issetArgument = true;
                }
            }

            if (!$issetArgument) {
                $inputArguments[] = null;
            }
        }

        return $inputArguments;
    }

    public function validateTimeArgument(string $time): bool
    {
        $isValid = true;

        try {
            new \DateTime($time);
        } catch (\Throwable $exception) {
            $isValid = false;
        }

        return $isValid;
    }
}
