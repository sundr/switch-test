<?php

namespace Api;

class JsonApi
{
    public const METHOD_GET  = 'GET';
    public const METHOD_POST = 'POST';

    /** @var string */
    private $apiUrl;

    public function __construct(array $config)
    {
        $this->apiUrl = $config['API_URL'] ?? '';
    }

    public function sendRequest(
        string $endpoint,
        string $method = self::METHOD_GET,
        array $data = [],
        array $queryParams = []
    ): array {
        $url = $this->apiUrl . $endpoint;

        if ($method === self::METHOD_GET) {
            $queryParams = array_merge($data, $queryParams);
        }

        if ($queryParams) {
            $url .= '?' . http_build_query($queryParams);
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);

        if ($method === self::METHOD_POST) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);
    }
}
