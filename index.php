<?php

$configs = parse_ini_file(__DIR__ . '/config/config.ini');

date_default_timezone_set($configs['date.timezone']);

require_once __DIR__ . '/vendor/autoload.php';

$cli = new Cli\Cli();
list($time, $genre) = $cli->parseInputArguments(['time', 'genre']);

if (!$time || !$genre) {
    fwrite(STDERR, 'Please provide 2 input arguments: "--time" and "--genre"' . PHP_EOL);
    exit(1);
}

if (!$cli->validateTimeArgument($time)) {
    fwrite(STDERR, 'Please provide input argument "--time" in right time format' . PHP_EOL);
    exit(1);
}

$movieService = new App\Service\MovieService($configs);
$movies = $movieService->fetchMovies();
$recommendations = $movieService->getMoviesRecommendations($movies, $genre, $time);

echo $movieService->getRecommendationsOutputString($recommendations);
